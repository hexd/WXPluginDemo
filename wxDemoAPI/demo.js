const express = require('express')
const app = express()
var request = require("request");

app.get('/', (req, res) => {
	// 小程序里调用wx.login返回的code
	const code = req.query.code;
	// 小程序appId，公众号管理后台可查
	const appId = 'xxxxxx';
	// 小程序secret，公众号管理后台可查
	const secret = 'xxxxxx';
	// http配置
	var options = {
		// 微信官方URL，用code换取openID
		url: `https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${secret}&js_code=${code}&grant_type=authorization_code`
	};
	
	request.get(options, function(err, response, body){
		// 返回openId等信息
		res.send(response.body)
	});
})

// express监听8601，自行修改
app.listen(8601, () => {
	console.log("8601端口已开启监听");
})