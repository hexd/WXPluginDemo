# WXPluginDemo

本项目为吾来智能机器人的简单使用Demo

WxDemo下是微信小程序，具有授权功能，并自动初始化一个机器人。

该小程序使用了吾来智能机器人小程序插件，关于如何添加小程序插件可参考下述文档：

https://mp.weixin.qq.com/wxopen/plugindevdoc?appid=wx4a2348285b702ed2

WxDemoAPI下是基于node的服务端代码，启动后会提供一个API，作用是用微信小程序提供的code换取openID，小程序端获取openId后，把它作为插件初始化的参数传入。