
//index.js
//获取应用实例
const app = getApp()
const wulAiSDK = require('../../utils/WulAiSDK.js'); /*根据实际引用地址配置*/
const myPluginInterface = requirePlugin('myPlugin');

import {
  service
} from '../../utils/service.js';

Page({
  data: {
    motto: '体验程序需要获取您的用户信息',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  loadSDK(){
    const userInfo = app.globalData.userInfo;
    const code = app.globalData.code;
    service.getSessionKey(code).then(
      res => {
        const pubkey = 'fTsiYBm0RcxjB6rAVGe8OCEkLK3EVjDi00df9a2624abbdbb38';
        const userinfo = {
          "avatar": userInfo.avatarUrl,
          "nickName": userInfo.nickName,
          "openid": res.data.openid
        }
        myPluginInterface.init(wulAiSDK, wx, null, pubkey, 'prod', userinfo, getApp(), true);
        this.setData({
          userInfo: userInfo,
          hasUserInfo: true
        })
      }
    );
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.loadSDK();
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.loadSDK();
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          this.loadSDK();
        }
      })
    }
  },
  getUserInfo: function (e) {
    app.globalData.userInfo = e.detail.userInfo;
    this.loadSDK();
  }
})