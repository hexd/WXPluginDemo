const service = {
  getSessionKey: function (code){
    return new Promise(
      function (resolve, reject) {
        wx.request({
          url: 'https://pmtest.wul.ai/wxplugin/',
          method: 'GET',
          data:{
            code:code,
          },
          success(res) {
            resolve(res);
          }
        })
      }
    )
  }
}

module.exports = {
  service: service
}